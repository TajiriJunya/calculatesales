package jp.alhinc.tajiri_junya.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args) {

		if( args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String,String> branchName = new HashMap<String, String>();
		HashMap<String,Long> branchUriage = new HashMap<String, Long>();
		//支店定義ファイル読み込み処理
		BufferedReader br = null;

		if(!branchRead(branchName,branchUriage,args[0],"branch.lst") ) {
			return;
		}

		//ここから売り上げ読み込み処理
		File[] Uriage = new File(args[0]).listFiles();

		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0;i < Uriage.length ; i++) {
			String fileName = Uriage[i].getName();
			if(Uriage[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(Uriage[i]);
			}
		}

		for(int i = 0; i < rcdFiles.size()-1; i++) {
			File Name = rcdFiles.get(i);
			File Name1 = rcdFiles.get(i+1);
			String SName = Name.getName();
			String SName1 = Name1.getName();
			String CutName = SName.substring(0,8);
			String CutName1 = SName1.substring(0,8);
			int IName = Integer.parseInt(CutName);
			int IName1 = Integer.parseInt(CutName1);
			if((IName1 - IName) != 1 ) {
				System.out.println("連番ではありません");
				return;
			}
		}
		//.rcdのファイルの数だけ繰りかえす
		for(int i = 0; i< rcdFiles.size(); i++) {

			try {
				//ファイル読み込みの準備
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));
				//文字列を一旦入れておくための”箱”を作成
				ArrayList<String> fileContents = new ArrayList<>();
				//”箱”の文字列スペースを確保
				String line = "";

				while((line = br.readLine()) != null) {
					//ファイルを読み込み文字列を1行ずつ”箱”に格納
					fileContents.add(line);
				}
				//行数チェック
				if(fileContents.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}
				//支店コードチェック
				if(!branchName.containsKey(fileContents.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//支店コードの処理
				//支店コードのみ別の箱に格納
				String branchCode = fileContents.get(0);

				//売上金額のエラー処理
				String Uriagegaku = fileContents.get(1);
				if(!Uriagegaku.matches("^[0-9]{1,}$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//売上の加算
				//売り上げの値をLong型に変換
				long fileSale = Long.parseLong(fileContents.get(1));
				//Mapの売り上げファイルより呼び出した値と加算
				Long saleAmount = branchUriage.get(branchCode) + fileSale;

				if (!(saleAmount < 10000000000L)) {
					System.out.println("合計金額が10桁を超えています");
					return;
				}
				//加算済みの売り上げの値をMapに返す
				branchUriage.put(branchCode, saleAmount);

			}
			catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
			if(!branchWrite(branchName,branchUriage,args[0],"branch.out")) {
				return;
			}
		}


	}
	private static boolean branchRead(HashMap<String,String> Name,HashMap<String,Long> Uriage,String path,String fileName) {
		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			br = new BufferedReader(new FileReader(file));

			String line1;
			while((line1 = br.readLine()) != null) {
				String[] items = line1.split(",");
				if (!items[0].matches("^[0-9]{3}$") || items.length != 2) {
					 System.out.println("ファイルフォーマットが不正です");
					 return false;
				}
				Name.put(items[0],items[1]);
				Uriage.put(items[0],0L);
				//System.out.println(line);
			}
			//System.out.println(branchUriage);

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	private static boolean branchWrite(HashMap<String,String> Name,HashMap<String,Long> Uriage,String path,String fileName2) {
		BufferedWriter bw = null;

		try {
			File file = new File(path,fileName2);
			bw = new BufferedWriter ( new FileWriter(file));

			for(String key: Name.keySet()) {
				bw.write( key + "," + Name.get(key) + "," + Uriage.get(key));
				bw.newLine();
			}
		}

		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}

		finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}


